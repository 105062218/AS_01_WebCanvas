var marker = "rgb(0,0,0)";
var markerWidth = 1;

var lastE;
var newPoint;
var pointN = 1;
var startPoint;
var mouseDown = false;
var mode = "draw";
var text;
var fontSize = "20px";
var typeFace = "Arial";
var imgURL;
var img = new Image();
var preview;
var threeP = new Array();

var c = document.getElementById("canvas");
var context = c.getContext("2d");

var clear = document.getElementById("clear");
var eraser = document.getElementById("eraser");
var pencil = document.getElementById("pencil");
var type = document.getElementById("type");
var line = document.getElementById("line");
var m = document.getElementById("marker");

var buttons = document.querySelector(".btn-primary");

var imgData;
var h = new Array();
var r = new Array();

context.rect(0, 0, c.offsetWidth,c.offsetHeight);
context.fillStyle = "#FFF";
context.fill();


c.addEventListener('mousedown',function(e){
    if(mode!="text"&&mode!="upload"&&mode!="uploadFile"&&mode!="T"){
        lastE = e;
        mouseDown = true;
        context.beginPath();
        context.moveTo(e.offsetX,e.offsetY);
        context.lineTo(e.offsetX,e.offsetY);
        context.lineCap='round';
        if(mode==="draw"){
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
        }
        if(mode==="erase"){
            context.strokeStyle = "#FFF";
            context.lineWidth=markerWidth;
        }
        if(mode==="line"||mode==="rect"||mode==="circle"||mode==="tri"){
            imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
        }
        if(mode==="tri"){
            newPoint = e;
            if(pointN==1){
                startPoint = e;
            }
        }
        context.stroke();
    }

});

c.addEventListener('mousemove', function(e){
    if(mouseDown){
        if(mode==="line"){
            context.putImageData(imgData,0,0);
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
            context.beginPath();
            context.moveTo(lastE.offsetX,lastE.offsetY);
            context.lineTo(e.offsetX,e.offsetY);
            context.lineCap='round';
        }
        if(mode==="draw"){
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
            context.beginPath();
            context.moveTo(lastE.offsetX,lastE.offsetY);
            context.lineTo(e.offsetX,e.offsetY);
            context.lineCap='round';
        }
        if(mode==="erase"){
            context.strokeStyle = "#FFF";
            context.lineWidth=markerWidth;
            context.beginPath();
            context.moveTo(lastE.offsetX,lastE.offsetY);
            context.lineTo(e.offsetX,e.offsetY);
            context.lineCap='round';
        }
        if(mode==="rect"){
            context.putImageData(imgData,0,0);
            context.beginPath();
            context.moveTo(lastE.offsetX,lastE.offsetY);
            context.lineTo(lastE.offsetX,e.offsetY);
            context.moveTo(lastE.offsetX,e.offsetY);
            context.lineTo(e.offsetX,e.offsetY);
            context.moveTo(e.offsetX,e.offsetY);
            context.lineTo(e.offsetX,lastE.offsetY);
            context.moveTo(e.offsetX,lastE.offsetY);
            context.lineTo(lastE.offsetX,lastE.offsetY);
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
        }
        if(mode==="circle"){
            context.putImageData(imgData,0,0);
            context.beginPath();
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
            var rad = Math.sqrt(Math.pow(lastE.offsetX-e.offsetX,2)+Math.pow(lastE.offsetY-e.offsetY,2));
            context.arc(lastE.offsetX,lastE.offsetY,rad,0,2*Math.PI);
        }
        if(mode==="tri"){
            context.putImageData(imgData,0,0);
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
            context.beginPath();
            context.moveTo(lastE.offsetX,lastE.offsetY);
            context.lineTo(e.offsetX,e.offsetY);
            context.lineCap='round';
        }
        context.stroke();
        if(mode!="line"&&mode!="rect"&&mode!="circle"&&mode!="tri"){
            lastE = e;
        }
        if(mode==="tri"){
            newPoint = e;
        }
    }
});

c.addEventListener('mouseup',function(){
    if(mode!="tri"){
        mouseDown = false;
    }
    if(mode==="tri"){
        lastE = newPoint;
        pointN++;
        var a = lastE.offsetX-startPoint.offsetX;
        var b = lastE.offsetY-startPoint.offsetY;
        var d = Math.sqrt(a*a+b*b);
        var buffer = markerWidth;
        if(markerWidth<5){
            buffer+=3;
        }
        if(d<=buffer&&pointN>2){
            mouseDown = false;
            lastE = startPoint;
            pointN=1;
            context.putImageData(imgData,0,0);
            context.stroke();
            //imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
            h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));

        }
        else{
            context.putImageData(imgData,0,0);
            context.lineWidth=markerWidth;
            context.stroke();
            imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
            //h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));
        }
    }
    if(mode==="line"||mode==="rect"||mode==="circle"){
        context.putImageData(imgData,0,0);
        context.stroke();
        imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
        h.push(imgData);
    }
    if(mode==="draw"||mode==="erase"){
        imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
        h.push(imgData);
    }

});

c.addEventListener('mouseout',function(){
    mouseDown = false;
    pointN=1;
    threeP = new Array();
});

c.addEventListener('click', function(e){
    if(mode=="text"){
        context.font= fontSize + " " + typeFace;
        context.fillStyle = marker;
        context.fillText(text,e.offsetX,e.offsetY);
        //imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
        h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));
    }
    if(mode=="upload"){
        //var img = new Image();
        img.src = imgURL;
        //img.onload = function(){
            context.drawImage(img,e.offsetX,e.offsetY);
            //imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
            h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));
        //}
        //imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
        //h.push(imgData);
        //h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));
    }
    if(mode=="uploadFile"){
        //var img = new Image();
        //img.src = preview.src;
        //if(preview.width>1000){
            //img.width=800;
        //}
        //preview.onload = function(){
            context.drawImage(preview,e.offsetX,e.offsetY);
            //imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
            h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));
        //}
        //imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
        //h.push(imgData);
    }
    if(mode=="T"){
        threeP.push(e);
        if(threeP.length==3){
            context.beginPath();
            context.strokeStyle = marker;
            context.lineWidth=markerWidth;
            context.lineJoin='miter';
            context.moveTo(threeP[0].offsetX,threeP[0].offsetY);
            context.lineTo(threeP[1].offsetX,threeP[1].offsetY);
            context.lineTo(threeP[2].offsetX,threeP[2].offsetY);
            //context.lineTo(threeP[0].offsetX,threeP[0].offsetY);
            //context.lineTo(threeP[1].offsetX,threeP[1].offsetY);
            //context.lineJoin='miter';
            context.closePath();
            context.stroke();
            h.push(context.getImageData(0,0,c.offsetWidth,c.offsetHeight));
            threeP = new Array();
        }
    }

});

clear.addEventListener('click',function(){
    context.clearRect(0,0,c.offsetWidth,c.offsetHeight);
    context.beginPath();
    context.rect(0, 0, c.offsetWidth,c.offsetHeight);
    context.fillStyle = "#FFF";
    context.fill();
    imgData = context.getImageData(0,0,c.offsetWidth,c.offsetHeight);
    h.push(imgData);
});

eraser.addEventListener('click',function(){
    mode = "erase";
    selectBtn(this);
});

pencil.addEventListener('click',function(){
    mode = "draw";
    c.style.cursor = "default";
    selectBtn(this);
});

line.addEventListener('click',function(){
    mode = "line";
    c.style.cursor = "crosshair";
    selectBtn(this);
});

function changeWidth(val){
    markerWidth = val;
}

function changeColor(val){
    marker = val;
}

function getText(val){
    text = val;
}

function changeFontSize(val){
    fontSize = val+"px";
}

function getFont(val){
    typeFace = val;
}

function undo(){
    if(h.length!=0){
        r.push(h.pop());
        imgData = h.pop();
        context.clearRect(0,0,c.offsetWidth,c.offsetHeight);
        context.rect(0, 0, c.offsetWidth,c.offsetHeight);
        context.fillStyle = "#FFF";
        context.fill();
        //context.putImageData(history,0,0);
        //context.stroke();
        context.putImageData(imgData, 0, 0);
        h.push(imgData);
    }
}

function redo(){
    if(r.length!=0)
    {
        imgData = r.pop();
        context.clearRect(0,0,c.offsetWidth,c.offsetHeight);
        context.rect(0, 0, c.offsetWidth,c.offsetHeight);
        context.fillStyle = "#FFF";
        context.fill();
        context.putImageData(imgData, 0, 0);
        h.push(imgData);
    }
}

function rect(){
    mode = "rect";
    c.style.cursor = "crosshair";
    selectBtn(document.getElementById("rect"));
}

function circle(){
    mode = "circle";
    c.style.cursor = "crosshair";
    selectBtn(document.getElementById("circle"));
}

function tri(){
    mode = "tri";
    pointN = 1;
    c.style.cursor = "crosshair";
    selectBtn(document.getElementById("tri"));
}

function T(){
    mode = "T";
    selectBtn(document.getElementById("T"));
    c.style.cursor = "crosshair";
    threeP = new Array();
}

function addText(){
    mode = "text";
    c.style.cursor = "text";
    selectBtn(document.getElementById("type"));
}

function download(link){
    link.href = c.toDataURL();
    link.download = "test.png";
}

document.getElementById('download').addEventListener('click', function() {
    download(this);
});

/*function upload(){
    var x=document.getElementById("file");
    if ('files' in x) {
        if (x.files.length != 0) {
            for (var i = 0; i < x.files.length; i++)
            {
                var file = x.files[i];
                if ('filepath' in file)
                {
                    alert(file.filepath);
                }
            }
        }
    } 
}*/

function getURL(val){
    imgURL = val;
    //img.src = imgURL;
}

function upload(){
    mode = "upload";
    img.src = imgURL;
    c.style.cursor = "copy";
    selectBtn(document.getElementById("upload"));
}

function uploadFile() {
    mode = "uploadFile";
    c.style.cursor = "copy";
    selectBtn(document.getElementById("paste"));
    preview = document.querySelector('img');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();
  
    reader.addEventListener("load", function () {
      preview.src = reader.result;
    }, false);
  
    if (file) {
      reader.readAsDataURL(file);
    }
}

function paste(){
    uploadFile();
    //selectBtn(document.getElementById("paste"));
}

function selectBtn(btnID){
    buttons = document.querySelector(".btn-primary")
    buttons.classList.add("btn-default");
    buttons.classList.remove("btn-primary");
    btnID.classList.add("btn-primary");
    btnID.classList.remove("btn-default");
}



